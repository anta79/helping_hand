import React ,{useContext,useEffect,useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import {
  Container,
  Card,
  UserInfo,
  UserImg,
  UserName,
  UserInfoText,
  PostTime,
  PostText,
  PostImg,
  InteractionWrapper,
  Interaction,
  InteractionText,
  Divider,
} from '../styles/FeedStyles';
import { AuthContext } from '../navigation/AuthProvider';
import showToast from '../toaster'

const PostCard = ({item,func}) => {
  const {user} = useContext(AuthContext);

  likeIcon = item.liked ? 'heart' : 'heart-outline';
  likeIconColor = item.liked ? '#2e64e5' : '#333';
  var [pic,setPic] = useState(require('../assets/users/user-3.jpg'))
  var [postImg,setpostImg] = useState('none')
  function done2() {
    let imageRef = storage().ref(item.uid);
    imageRef.getDownloadURL().then((url) => {
      setPic({uri: url})
      // console.log(url)
    }).catch((e) => console.log('getting downloadURL of image error => ', e));
    // console.log(item.uid)
  }
  
  function done3() {
    let imageRef = storage().ref(item.uid+item.id);
    imageRef.getDownloadURL().then((url1) => {
      setpostImg({uri: url1})
      // console.log(url1)
    }).catch((e) => console.log('getting downloadURL of postImage error => ', e));
    // console.log(item.uid)
  }
  if (item.likes == 1) {
    likeText = '1 Like';
  } else if (item.likes > 1) {
    likeText = item.likes + ' Likes';
  } else {
    likeText = 'Like';
  }

  if (item.comments == 1) {
    commentText = '1 Comment';
  } else if (item.comments > 1) {
    commentText = item.comments + ' Comments';
  } else {
    commentText = 'Comment';
  }
  useEffect(() => {
    done2();
    done3();
  },[])
  return (
    <Card>
      <UserInfo>
        <UserImg source={pic} />
        <UserInfoText>
          <UserName>{item.userName}</UserName>
          <PostTime>{item.postTime}</PostTime>
        </UserInfoText>
        
      </UserInfo>
      <PostText>{item.post}</PostText>
      {postImg != 'none' ? <PostImg source={postImg} /> : <Divider />}

      <InteractionWrapper>
        {user.uid === item.uid ? 
          <Interaction active={false} onPress={() => {
              showToast("deleting")
              firestore().collection('posts').doc(item.extra).delete().then(() => {
                  storage().ref(item.uid+item.id).delete().then(()=> {
                    func()
                  }).catch(e => func())
              });
          }}>
            <Ionicons name="trash" size={25}  />
            <InteractionText active={false}>{"Delete"}</InteractionText>
          </Interaction> :
          <Interaction active={item.liked}>
              <Ionicons name={likeIcon} size={25} color={likeIconColor} />
              <InteractionText active={item.liked}>{likeText}</InteractionText>
          </Interaction>
        }
        <Interaction>
          <Ionicons name="md-chatbubble-outline" size={25} />
          <InteractionText>{commentText}</InteractionText>
        </Interaction>
      </InteractionWrapper>
    </Card>
  );
};

export default PostCard;

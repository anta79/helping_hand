import React ,{useState,useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

import firestore from '@react-native-firebase/firestore';
import {
  Container,
  Card,
  UserInfo,
  UserImg,
  UserName,
  UserInfoText,
  PostTime,
  PostText,
  PostImg,
  InteractionWrapper,
  Interaction,
  InteractionText,
  Divider,
} from '../styles/FeedStyles';

const NewCard = ({item}) => {
  let [k,setK] = useState("Loading")
  async function done() {
    const user_ = await firestore().collection('users').doc(item.uid).get();
    setK(user_.data().name) 
  }
  useState(() => {
    done()
    console.log(k)
  },[])
  return (
    <Card>
      <UserInfo>
        <UserInfoText>
          <UserName>{k}</UserName>
        </UserInfoText>
      </UserInfo>
      <PostText>{item.post}</PostText>
    </Card>
  );
};

export default NewCard;

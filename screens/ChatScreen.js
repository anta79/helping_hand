import React from 'react';
import { View, Text, Button, StyleSheet} from 'react-native';


const ChatScreen = () => {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Future Work</Text>
      </View>
    );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  text : {
    fontSize : 25,
    color : '#737373'
  },
});

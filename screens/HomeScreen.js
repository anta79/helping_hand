import React ,{useState,useEffect,useRef}from 'react';
import {View, Text,ToastAndroid, StyleSheet, FlatList,TouchableOpacity} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import PostCard from '../components/PostCard';
import NewCard from '../components/newCard';
import {
  Container,
} from '../styles/FeedStyles';


import firestore from '@react-native-firebase/firestore';


const HomeScreen = ({navigation}) => {
   
  let [posts,setPosts] = useState([])
  function onRefress() {

    firestore().collection('posts')
    .orderBy('createdAt','desc').get().then(querySnapshot => {
        let temp = []
        querySnapshot.forEach(documentSnapshot => {
          let b =  documentSnapshot.id
          let d = documentSnapshot.data()
          
          temp.push({
            id: d.time,
            userName: d.name,
            userImg: require('../assets/users/user-3.jpg'),
            postTime: d.time,
            post: d.post,
            postImg: 'none',
            liked: false,
            likes: '0',
            comments: '0',
            uid : d.uid,
            extra : b,
          })
          
        })
        setPosts(temp)
    });
  }
  function showToast(msg) {
    ToastAndroid.showWithGravityAndOffset(
      msg,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  };
  useEffect(()=> {
    onRefress();
  },[])

  const flatListRef = useRef()
  const toTop = () => {
    // use current
    flatListRef.current.scrollToOffset({ animated: true, offset: 0 })
  }
  return (
    <Container>
      <FlatList 
        ref={flatListRef}
        data={posts}
        renderItem={({item}) =>  <PostCard item={item} func={onRefress} />}
        keyExtractor={item=>(item.id)}
        showsVerticalScrollIndicator={false}
      />
      <TouchableOpacity style={styles.fab} onPress={ async ()=>{showToast("refreshing"); await onRefress() ;toTop()}}>
          <Icon name="refresh" size={20} style={{color: 'white'}} />
      </TouchableOpacity>
    </Container>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  fab:{
    height: 50,
    width: 50,
    borderRadius: 200,
    position: 'absolute',
    bottom: 20,
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#686cc3',
  },
})

import React, { useContext, useState ,useLayoutEffect} from 'react';
import {View, Text, Button, StyleSheet,TouchableOpacity,Alert,ToastAndroid} from 'react-native';
import {InputField, InputWrapper} from '../styles/AddPost';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import firestore from '@react-native-firebase/firestore';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import { AuthContext } from '../navigation/AuthProvider';
import shortToast from '../toaster'
var time;

const AddPostScreen = ({navigation}) => {
  const [post,setPost] = useState("")
  const {user} = useContext(AuthContext);
  const [image, setImage] = useState(null);

  function showToast(msg) {
    ToastAndroid.showWithGravityAndOffset(
      msg,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      50
    );
  };
  

  const selectImage = () => {
      const options = {
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
      skipBackup: true,
      path: 'images'
      }
    };

   ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        setImage(source);
        shortToast("image selected")
      }
    });
  };
  

  async function uploadImage() {
        const { uri } = image;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const filename = user.uid + time ;
        showToast("Posting ...")
        storage().ref(filename).putFile(uploadUri).then(()=> {
          console.log("nani")
          pressHandler();
        }).catch(err=> pressHandler())
    };

  async function pressHandler() {

    let data = await firestore().collection('users').doc(user.uid).get();
    firestore().collection('posts').add({
      uid: user.uid,
      name : data.data().name ,
      post: post,
      time : time,
      createdAt : firestore.FieldValue.serverTimestamp() 
    }).then(() => {
      console.log('Post added!');
      navigation.navigate("Helping Hand")
    });
  }


  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={{marginRight: 15}}>
          <TouchableOpacity onPress={()=> {
            if(post.trim() === ""){
              showToast("Please write something")
            }
            else {
              var tem = new Date(Date.now()) 
              time =  tem.toLocaleTimeString() + "    " + tem.toLocaleDateString(),
              // time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()
              uploadImage() ;
            }
          }}>
            <Text style={{fontWeight:'600', fontSize: 18, color: '#2e64e5'}}>Post</Text>
          </TouchableOpacity>
        </View>
      ),
    });
  }, [post,image]);




  return (
    <View style={styles.container}>
      <InputWrapper>
        <InputField placeholder="Say to the world?"  multiline numberOfLines={4}
            value = {post} onChangeText={(text)=>{setPost(text); if(image === null) {setImage({uri : ""})}}}
        />
      </InputWrapper>
      <TouchableOpacity style={styles.fab} onPress={selectImage}>
          <Icon name="image" size={20} style={{color: 'white'}} />
      </TouchableOpacity>

    </View>
  );
};

export default AddPostScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  fab:{
    height: 55,
    width: 55,
    borderRadius: 200,
    position: 'absolute',
    bottom: 20,
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#22d059',
  },
});

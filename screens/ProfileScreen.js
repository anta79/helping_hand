import React, {useContext, useEffect,useState} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FormButton from '../components/FormButton';
import UploadScreen from './UploadScreen'
import styled from 'styled-components';
import { AuthContext } from '../navigation/AuthProvider';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';

const ProfileScreen = () => {
  var [k,setK] = useState("Loading...")
  var [pic,setPic] = useState(require('../assets/users/user-3.jpg'))
  const {user, logout,getUser} = useContext(AuthContext);
  async function done() {
    const user_ = await firestore().collection('users').doc(user.uid).get();
    setK(user_.data().name) 
    console.log(k)
  }

  function done2() {
    var imageRef = storage().ref(user.uid);
    imageRef.getDownloadURL().then((url) => {
      //from url you can fetched the uploaded image easily
      setPic({uri: url})
    }).catch((e) => console.log('getting downloadURL of image error => ', e));
  }

  useEffect(() => {
    done() ;
    console.log(pic)
    done2();
  },[])
  return (
    <View style={styles.container}>
      <UploadScreen uid={user.uid} call={done2}/>
      <View style={{flex : 1 ,width:'100%',flexDirection : 'row' ,justifyContent:'center',alignItems:'center'}}>
          <UserImg source={pic}  />
          <Text style={styles.text}>{k}</Text>
      </View>
      <FormButton buttonTitle="Logout" bgcolor = "#e57091" onPress={() => logout()} />
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eee' ,
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontSize: 18,
    color: '#333333'
  }
});

const UserImg = styled.Image`
    width: 100px;
    height: 100px;
    margin-right : 40px;
    border-radius: 50px;
`;
import React, {createContext, useState} from 'react';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import firestore from '@react-native-firebase/firestore'; 
import showToast from '../toaster'
export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);

  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        login: async (email, password) => {
          try {
            await auth().signInWithEmailAndPassword(email, password);
          } catch (e) {
            console.log(e);
            showToast("Wrong credentials")
          }
        },
        googleLogin: async () => {
          try {
            // Get the users ID token
            const { idToken } = await GoogleSignin.signIn();

            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);
            // Sign-in the user with the credential
            await auth().signInWithCredential(googleCredential);
            // console.log(auth().currentUser.displayName)
            
            firestore().collection('users').doc(auth().currentUser.uid).set({
              name: auth().currentUser.displayName,
            }).then(() => {
                console.log('User added!');
            });
          } catch(error) {
            console.log({error});
          }
        },
        fbLogin: async () => {
          try {
            // Attempt login with permissions
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (result.isCancelled) {
              throw 'User cancelled the login process';
            }

            // Once signed in, get the users AccesToken
            const data = await AccessToken.getCurrentAccessToken();

            if (!data) {
              throw 'Something went wrong obtaining access token';
            }

            // Create a Firebase credential with the AccessToken
            const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

            // Sign-in the user with the credential
            await auth().signInWithCredential(facebookCredential);
            firestore().collection('users').doc(auth().currentUser.uid).set({
              name: auth().currentUser.displayName,
            }).then(() => {
                console.log('User added!');
            });
          } catch(error) {
            console.log({error});
          }
        },
        register: async (email,name,password) => {
          try {
            await auth().createUserWithEmailAndPassword(email, password);
            // console.log(auth().currentUser.uid)
            firestore().collection('users').doc(auth().currentUser.uid).set({
                name: name,
            }).then(() => {
              console.log('User added!');
            });
          }catch (e) {
            console.log(e);
          }
        },
        getUser : () => {
          return auth().currentUser
        },
        logout: () => {
          // try {
            
            LoginManager.logOut();
            // await GoogleSignin.revokeAccess();
            // await GoogleSignin.signOut() ;
            // await auth().signOut();
            auth().signOut().then(()=> {
              GoogleSignin.revokeAccess().then(()=> {
                GoogleSignin.signOut().then(() => {
                  console.log("success")
                }).catch(err=> LoginManager.logout());
              }).catch(err => LoginManager.logout())
            }).catch(err => console.log(err))
             
          }, 
          // catch (e) {
          //   console.log(e);
          // }
        
      }}>
      {children}
    </AuthContext.Provider>
  );
};
